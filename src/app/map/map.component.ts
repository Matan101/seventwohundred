import { Component } from '@angular/core';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.less'],
})
export class MapComponent {
  flyToOptions = {
    duration: 0,
    destination: Cesium.Cartesian3.fromRadians(0.6, 0.55, 690000),
  };

  constructor() {}
}
