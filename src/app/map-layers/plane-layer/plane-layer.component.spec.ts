import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaneLayerComponent } from './plane-layer.component';

describe('PlaneLayerComponent', () => {
  let component: PlaneLayerComponent;
  let fixture: ComponentFixture<PlaneLayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaneLayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaneLayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
