import { Component, OnInit } from '@angular/core';
import {
  ActionType,
  AcEntity,
  AcNotification,
  ViewerConfiguration,
} from 'angular-cesium';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { MessageComponent } from 'src/app/message/message.component';
import { PLANE_INITIAL_DIRECTION, PLANE_INITIAL_POSITION } from './plane.data';

@Component({
  selector: 'plane-layer',
  templateUrl: './plane-layer.component.html',
  styleUrls: ['./plane-layer.component.less'],
  providers: [ViewerConfiguration],
})
export class PlaneLayerComponent implements OnInit {
  planeDirection: number = PLANE_INITIAL_DIRECTION;
  planePosition = PLANE_INITIAL_POSITION;

  plane$ = new Subject<AcNotification>();

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {
    setInterval(() => {
      this.updatePlanes();
      this.subscribeToPlaneInsideAmat();
      this.updatePosition();
    }, 100);
  }

  updatePlanes(): void {
    this.plane$.next({
      id: '1',
      actionType: ActionType.ADD_UPDATE,
      entity: AcEntity.create({
        image: '../../../assets/images/plane.png',
        position: Cesium.Cartesian3.fromDegrees(
          this.planePosition.x,
          this.planePosition.y,
          null
        ),
        rotation: -1 * Cesium.Math.toRadians(this.planeDirection),
      }),
    });
  }

  updatePosition(): void {
    this.planePosition = {
      x: this.planePosition.x + (this.planeDirection > 0 ? 0.004 : -0.004),
      y: this.planePosition.y + (this.planeDirection > 0 ? 0.005 : -0.005),
    };
  }

  changeDirection(): void {
    this.planeDirection = -160;
  }

  showAmatMessage(): void {
    const dialogRef = this.dialog.open(MessageComponent);
    dialogRef.afterOpened().subscribe(() => this.changeDirection());
  }

  // TODO: write method that checks if the plane is inside the amat
  isPlaneInsideTheAmat(): boolean {
    return false;
  }

  subscribeToPlaneInsideAmat(): void {
    if (this.isPlaneInsideTheAmat()) {
      this.showAmatMessage();
      this.changeDirection();
    }
  }
}
