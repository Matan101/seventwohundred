import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmatLayerComponent } from './amat-layer.component';

describe('AmatLayerComponent', () => {
  let component: AmatLayerComponent;
  let fixture: ComponentFixture<AmatLayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmatLayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AmatLayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
