import { Component, OnInit } from '@angular/core';
import { AcEntity, AcNotification, ActionType } from 'angular-cesium';
import { Subject } from 'rxjs';
import { AMAT_POSITION, AMAT_RADIUS } from './amat-layer.data';

@Component({
  selector: 'amat-layer',
  templateUrl: './amat-layer.component.html',
  styleUrls: ['./amat-layer.component.less'],
})
export class AmatLayerComponent implements OnInit {
  amatPosition = AMAT_POSITION;
  amatRadius = AMAT_RADIUS;

  amat$ = new Subject<AcNotification>();

  constructor() {}

  ngOnInit(): void {
    setTimeout(() => this.initAmats(), 200);
  }

  initAmats(): void {
    this.amat$.next({
      id: '1',
      actionType: ActionType.ADD_UPDATE,
      entity: AcEntity.create({
        position: Cesium.Cartesian3.fromDegrees(
          this.amatPosition.x,
          this.amatPosition.y,
          null
        ),
        radius: this.amatRadius,
        material: Cesium.Color.fromAlpha(Cesium.Color.RED, 0.2),
      }),
    });
  }
}
